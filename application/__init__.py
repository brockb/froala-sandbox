from flask import Flask, render_template

def create_app():
  app = Flask(__name__)

  @app.route('/', methods=['GET'])
  @app.route('/froalav2', methods=['GET'])
  def froalav2():
    return render_template('froalav2.html')

  @app.route('/froalav3', methods=['GET'])
  def froalav3():
    return render_template('froalav3.html')

  return app